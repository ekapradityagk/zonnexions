//
//  SetingsViewController.h
//  splitBanana
//
//  Created by Technical Staff on 11/27/15.
//  Copyright © 2015 EP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MapViewController.h"
#import "SWRevealViewController.h"

@interface SetingsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>



@end
